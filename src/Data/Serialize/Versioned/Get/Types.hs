{-# LANGUAGE GeneralizedNewtypeDeriving, PolyKinds, DataKinds #-}
module Data.Serialize.Versioned.Get.Types where

import Control.Applicative (Alternative)
import Control.Monad (MonadPlus)
import Control.Monad.Trans.Reader (ReaderT)
import Data.Serialize (Get)
import Data.Word (Word64)

-- | A variant of 'Get' that encodes in its type which version is
-- requested to be deserialized and internally keeps track of which
-- version was encountered in the serialized data.
newtype VersionedGet d a = VersionedGet (ReaderT Word64 Get a)
  deriving (Functor, Applicative, Alternative, Monad, MonadFail, MonadPlus)
