{-|
Description : Defining series of versioned data structures
Copyright   : 2020 Sven Bartscher
License     : MPL-2.0
Maintainer  : sven.bartscher@weltraumschlangen.de
Stability   : experimental
Portability : GHC

This module deals with defining version domains, the versions in those
domains and the structures that can be serialized with those versions.
-}

module Data.Serialize.Versioned.Versioned
  ( -- * Version domains
    --
    -- | A version domain is a kind of namespace for
    -- versions. Whenever a version is mentioned in a type it is
    -- mentioned alongside a version domain. This reflects how version
    -- numbers are usually not a globally valid, but only valid in a
    -- given context such as a data format.
    --
    -- A version domain can be represented by any type of any
    -- kind. Usually you want to define type just for using it as a
    -- version domain, such as this:
    --
    -- @
    -- data MySaveFileFormat
    -- @
    --
    -- To actually use the type as a version domain you have to
    -- implement @'VersionDomain' MySaveFileFormat@.
    VersionDomain(..)
    -- * Version numbers
    --
    -- | Version numbers are represented as type level numbers of kind
    -- 'GHC.TypeLits.Nat'. Additionally they have to be smaller than
    -- 2^64-1 (2^64-1 itself is reserved for internal use).
  , DefinedVersion(..)
  , ValidVersion
    -- * VersionedSerialize class
  , VersionedSerialize(..)
    -- * VersionedGettable class
  , VersionedGettable(..)
    -- ** Method types
    --
    -- | The types of the methods 'migrate', 'versionedGet' and
    -- 'putVersioned' in 'VersionedSerialize' depend on the
    -- corresponding definition of 'MigrationType' as specified in the
    -- type families below.
  , MigrateType
  , VersionedGetType
    -- ** Migration variants
    --
    -- | There are various ways in which a structure in a specific
    -- version can relate to its previous representation in the
    -- preceeding version. 'MigrationType' represents the different
    -- variants possible.
  , MigrationType(..)
  , IsMigrationType
    -- *** Consistency checks
    --
    -- | When you define an instance of 'VersionedSerialize' the
    -- defined 'MigrationVariant' has to be consistent with
    -- surrounding instances, e.g. there might need to a preceeding
    -- version. These requirements are encoded in
    -- 'ConsistentMigrationVariant'.
  , ConsistentMigrationVariant
  , UnchangedRepresentation
  , IsPreviousRepresentation
  , HasPreviousVersion
  , ForcePreviousVersion
  , FromJust
    -- ** Generic serialization
    --
    -- | The below classes provide default implementations for generic
    -- types.
  , DefaultVersionedGet(..)
  , GVersionedGet(..)
  , GVersionedPut(..)
  ) where

import Data.Serialize.Versioned.Internal
