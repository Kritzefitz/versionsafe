{-# LANGUAGE PolyKinds, GeneralizedNewtypeDeriving, StandaloneDeriving, FlexibleInstances #-}
{-# LANGUAGE DataKinds #-}
module Data.Serialize.Versioned.Put.Types where

import Data.Serialize (PutM)

-- | A version of 'PutM' that checks if encoded values can actually be
-- encoded with the current version tag.
newtype VersionedPutM d v a =
  VersionedPutM { unVersionedPut :: PutM a }
  deriving (Monad, Functor, Applicative)

deriving instance Semigroup (VersionedPutM d v ())
deriving instance Monoid (VersionedPutM d v ()) 

-- | 'VersionedPutM' with an empty return type. Since serializing
-- values usually yields no result this is used very often.
type VersionedPut d v = VersionedPutM d v ()

-- | An operation that serializes a value of type @a@.
type VersionedPutter d v a = a -> VersionedPut d v
