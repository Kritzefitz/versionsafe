{-# LANGUAGE DeriveGeneric, DataKinds, MultiParamTypeClasses, TypeFamilies #-}
{-# LANGUAGE FlexibleInstances, FlexibleContexts, UndecidableInstances #-}
{-# OPTIONS_GHC -fno-warn-orphans #-}
module V0 where

import GHC.Generics (Generic)

import Data.Serialize ( put
                      , putListOf
                      )
import Data.Serialize.Versioned ( VersionDomain(CurrentVersion)
                                , VersionedSerialize( CurrentStructureVersion
                                                    , putVersioned
                                                    )
                                , VersionedGettable(MigrationVariant)
                                , MigrationType(Base)
                                , putUnversioned
                                , putUnversionedResumable
                                )
import Data.Word (Word64)

import Common

instance VersionDomain TestDomain where
  type CurrentVersion TestDomain = 0

instance ( VersionedSerialize TestDomain a
         , CurrentStructureVersion TestDomain a ~ 0
         ) => VersionedSerialize TestDomain [a] where
  putVersioned xs = putUnversionedResumable $ \resume ->
    putListOf (resume . putVersioned) xs

instance VersionedSerialize TestDomain Word64 where
  putVersioned = putUnversioned . put

instance VersionedSerialize TestDomain Char where
  putVersioned = putUnversioned . put

instance VersionedSerialize TestDomain Integer where
  putVersioned = putUnversioned . put

instance VersionedSerialize TestDomain Int where
  putVersioned = putUnversioned . put

data Foo = Foo Bar Baz
  deriving (Generic, Eq, Show)

instance VersionedGettable TestDomain 0 Foo where
  type MigrationVariant TestDomain 0 Foo = 'Base

instance VersionedSerialize TestDomain Foo

newtype Bar = Bar Word64
  deriving (Generic, Eq, Show)

instance VersionedGettable TestDomain 0 Bar where
  type MigrationVariant TestDomain 0 Bar = 'Base

instance VersionedSerialize TestDomain Bar

data Baz = Baz String BazSub
  deriving (Generic, Eq, Show)

instance VersionedGettable TestDomain 0 Baz where
  type MigrationVariant TestDomain 0 Baz = 'Base

instance VersionedSerialize TestDomain Baz

data BazSub = BazSub Integer | BazSubNone
  deriving (Generic, Eq, Show)

instance VersionedGettable TestDomain 0 BazSub where
  type MigrationVariant TestDomain 0 BazSub = 'Base

instance VersionedSerialize TestDomain BazSub
