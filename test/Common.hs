{-# LANGUAGE DataKinds, EmptyDataDecls, MultiParamTypeClasses, TypeFamilies, FlexibleInstances #-}
{-# LANGUAGE FlexibleContexts #-}
module Common where

import Data.Serialize.Versioned ( DefinedVersion(PreviousVersion)
                                , VersionedSerialize
                                , VersionedGettable( MigrationVariant
                                                   , versionedGet
                                                   )
                                , MigrationType ( Base
                                                , Unchanged
                                                )
                                )
import Data.Serialize.Versioned.Primitive (getPrimitiveVersioned)
import Data.Word (Word64)

data TestDomain

instance DefinedVersion TestDomain 0 where
  type PreviousVersion TestDomain 0 = 'Nothing

instance DefinedVersion TestDomain 1 where
  type PreviousVersion TestDomain 1 = 'Just 0

instance ( VersionedSerialize TestDomain a
         ) => VersionedGettable TestDomain 0 [a] where
  type MigrationVariant TestDomain 0 [a] = 'Base
  versionedGet = getPrimitiveVersioned

instance ( VersionedSerialize TestDomain a
         ) => VersionedGettable TestDomain 1 [a] where
  type MigrationVariant TestDomain 1 [a] = 'Unchanged

instance VersionedGettable TestDomain 0 Word64 where
  type MigrationVariant TestDomain 0 Word64 = 'Base
  versionedGet = getPrimitiveVersioned

instance VersionedGettable TestDomain 0 Char where
  type MigrationVariant TestDomain 0 Char = 'Base
  versionedGet = getPrimitiveVersioned

instance VersionedGettable TestDomain 1 Char where
  type MigrationVariant TestDomain 1 Char = 'Unchanged

instance VersionedGettable TestDomain 0 Integer where
  type MigrationVariant TestDomain 0 Integer = 'Base
  versionedGet = getPrimitiveVersioned

instance VersionedGettable TestDomain 1 Integer where
  type MigrationVariant TestDomain 1 Integer = 'Unchanged

instance VersionedGettable TestDomain 0 Int where
  type MigrationVariant TestDomain 0 Int = 'Base
  versionedGet = getPrimitiveVersioned
